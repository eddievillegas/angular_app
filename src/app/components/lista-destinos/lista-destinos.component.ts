import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = []
    this.store.select(state => state.destinos.favorito)
      .subscribe(destino => {
        if(destino != null) this.updates.push(`Se ha elegiho ${destino.nombre}`)
      });
    store.select(state => state.destinos.items).subscribe(items => this.all =  items);
  }

  agregado(destino: DestinoViaje): void {
    this.destinosApiClient.add(destino);
    this.onItemAdded.emit(destino);
  }

  elegido(destino: DestinoViaje): void {
    this.destinosApiClient.elegir(destino);

  }

  getAll(){

  }

  ngOnInit(): void {}

}
