import { Store } from '@ngrx/store';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { DestinoViaje } from './../models/destino-viaje.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destino-viajes.state.model';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})

export class DestinosApiClient {
    destinos: DestinoViaje[] = []
    constructor(private store: Store<AppState>, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, private http: HttpClient){
        this.store
            .select(state => state.destinos)
            .subscribe(data => {
                console.log('destinos sub store');
                console.log(data)
                this.destinos = data.items
            });
        this.store
            .subscribe(data => {
                console.log('all store')
                console.log(data);
            });
    }

    add(destino: DestinoViaje): void {
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', `${this.config.apiEndPoint}/my`, { nuevo: destino.nombre}, {headers});
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if(data.status === 200)
                this.store.dispatch(new NuevoDestinoAction(destino));
            const myDb = db;
            myDb.destinos.add(destino);
            console.log('todos los destinos de la db');
            myDb.destinos.toArray().then(destinos => console.log(destinos));
        })
    }

    elegir(destino: DestinoViaje): void {
        this.store.dispatch(new ElegidoFavoritoAction(destino));
    }

    getById(id: String): DestinoViaje {
        return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
    }
    
    getAll(){
        return this.destinos;
    }
}